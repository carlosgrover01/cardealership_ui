import { Component, OnInit } from '@angular/core';
import { Car } from './models/car';
import { CarsService } from './services/cars.service';
import {MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  carsList:Car[]=[];
  displayedColumns:string[] = ['brand','model','type'];
  dataSource = new MatTableDataSource<Car>(this.carsList);

  constructor(private carsService : CarsService) { }

  ngOnInit(): void {
    this.getCarsList();
  }

  getCarsList(){
    this.carsService.GetCars().subscribe(response =>this.dataSource.data = response as Car[] );
  } 

}
