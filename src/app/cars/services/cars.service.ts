import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

const CARS_API = environment.apiUrl + '/cars';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  constructor(private http: HttpClient) { }

  GetCars(){
    return this.http.get(CARS_API);
  }
}
