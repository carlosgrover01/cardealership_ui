import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarsRoutingModule } from './cars-routing.module';
import { CarsComponent } from './cars.component';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule} from '@angular/material/table'

@NgModule({
  declarations: [
    CarsComponent
  ],
  imports: [
    CommonModule,
    CarsRoutingModule,
    HttpClientModule,
    MatTableModule
  ]
})
export class CarsModule { }
